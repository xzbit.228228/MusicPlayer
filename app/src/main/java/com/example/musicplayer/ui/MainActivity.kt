package com.example.musicplayer.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.paddingFromBaseline
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.twotone.KeyboardArrowLeft
import androidx.compose.material.icons.twotone.KeyboardArrowRight
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.musicplayer.R
import com.example.musicplayer.ui.theme.MusicPlayerTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MusicPlayerTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.primary
                ) {
                    MainScreen()
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen() {
    Scaffold(
        topBar = { TopBar()},
        bottomBar = { MiniPlayer() }
    ) {contentPadding ->
        ContentLayout(modifier = Modifier.padding(contentPadding))
    }
}

@Composable
fun ContentLayout(modifier: Modifier) {
    val list = listOf(R.drawable.cover1,R.drawable.cover2, R.drawable.cover3, R.drawable.cover4)
    Surface(modifier = modifier.fillMaxWidth()) {
        Column(modifier = Modifier
            .padding(start = 20.dp)
            .verticalScroll(rememberScrollState())) {

            CardItem(title = "Recommended for you", elements = list)
            CardItem(title = "My Playlist", elements = list.reversed())

        }

    }
}
@Composable
fun TopBar() {
    var expanded by remember { mutableStateOf(false) }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 20.dp)
        ,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        IconButton(onClick = { expanded = !expanded}) {
            Icon(
                imageVector = Icons.Filled.Menu,
                contentDescription = "Menu Icon"

            )

        }
        IconButton(onClick = { println("Search") }) {
            Icon(
                imageVector = Icons.Filled.Search,
                contentDescription = "Search Icon"
            )
        }
    }
}
@Composable
fun MiniPlayer() {
    Column(modifier = Modifier
        .fillMaxWidth()
        .background(MaterialTheme.colorScheme.primary)) {
        Slider(value = 0.2f, onValueChange = {  }, colors = SliderDefaults.colors())
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(
                painter = painterResource(R.drawable.player_cover),
                contentDescription = "Player Cover",
                contentScale = ContentScale.FillWidth,
                modifier = Modifier
                    .height(71.dp)
                    .width(71.dp)
            )
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 8.dp)
            ) {
                Column(verticalArrangement = Arrangement.Center) {
                    Text(
                        text = "Chaff & Dust",
                        fontSize = 18.sp,
                        fontWeight = FontWeight(500),
                        fontFamily = FontFamily(Font(R.font.gilroy))
                    )
                    Text(
                        text = "HYONNA",
                        fontSize = 10.sp,
                        fontWeight = FontWeight(400),
                        color = MaterialTheme.colorScheme.tertiary
                    )
                }
                Row() {
                    IconButton(onClick = { /*TODO*/ }) {
                        Icon(imageVector = Icons.TwoTone.KeyboardArrowLeft, contentDescription = "Play Arrow")
                    }
                    IconButton(onClick = { /*TODO*/ }) {
                        Icon(imageVector = Icons.Filled.PlayArrow, contentDescription = "Play Arrow")
                    }
                    IconButton(onClick = { /*TODO*/ }) {
                        Icon(imageVector = Icons.TwoTone.KeyboardArrowRight, contentDescription = "Play Arrow")
                    }
                }

            }
        }
    }
}
@Composable
fun CardItem(title: String, elements: List<Int>) {
    Text(
        text = title,
        fontFamily = FontFamily(Font(R.font.gilroy_bold)),
        fontSize = 24.sp,
        fontWeight = FontWeight(700),
        modifier = Modifier.padding(top = 40.dp)
    )
    LazyRow(
        modifier = Modifier
            .fillMaxWidth()
            .paddingFromBaseline(top = 20.dp),
        horizontalArrangement = Arrangement.spacedBy(14.dp)
    ) {
        items(items = elements, itemContent = {item ->
            Image(
                painter = painterResource(item),
                contentDescription = "Cover1",
                contentScale = ContentScale.FillWidth,
                modifier = Modifier
                    .width(190.dp)
                    .height(190.dp)
                    .scale(1f)
                    .clip(RoundedCornerShape(8.dp))
            )
        })
    }
}

@Preview(showBackground = false)
@Composable
fun ScaffoldPreview() {
    MainScreen()
}

