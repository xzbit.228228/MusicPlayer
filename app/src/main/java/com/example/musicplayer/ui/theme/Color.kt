package com.example.musicplayer.ui.theme

import androidx.compose.ui.graphics.Color

val darkBlue = Color(0xFF091227)
val lightBlueText = Color(0xB2A5C0FF)
val darkBlueText = Color(0xFF091127)
val lightGrey = Color(0xFFF7FAFF)